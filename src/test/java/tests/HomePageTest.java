package tests;

import helpers.annotations.XmlFileParameters;
import helpers.implementations.XmlLoginHelper;
import listeners.ListenerClass;
import objects.LoginObject;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by IlyaMakarov on 1/18/2017.
 */
@Listeners({ ListenerClass.class })
public class HomePageTest {
    private WebDriver driver;
    private static final String selectMethod = "testMethod";

    private static final String topic = "testTopic";
    private static final String body = "testBody";

    private static final String LOGIN = "testautomation_ilya";
    private static final String PASSWORD = "asdf1234";

    private static final String USERNAME = "ilyamakarov";
    private static final String ACCESS_KEY = "b9da408b-81c9-44cd-b792-b41627eed052";
    private static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";


    @BeforeClass
    public void setUp() throws MalformedURLException {
        int testMethod = Integer.parseInt(System.getProperty(selectMethod));
        switch (testMethod) {
            case 1:// locally
                driver = new FirefoxDriver();
                break;
            case 2:// selenium grid
                DesiredCapabilities capability = DesiredCapabilities.firefox();
                String ip = System.getProperty("URL");
                String port = System.getProperty("port");
                driver = new RemoteWebDriver(new URL(ip + ":" + port + "/wd/hub"), capability);
                break;
            case 3:// saucelabs
                DesiredCapabilities caps = DesiredCapabilities.edge();
                caps.setCapability("platform", Platform.WIN10);
                driver = new RemoteWebDriver(new URL(URL), caps);

                break;
            default:
                driver = new FirefoxDriver();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                break;
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test(dataProvider = "loginData", dataProviderClass=XmlLoginHelper.class)
    @XmlFileParameters(path = "src/main/resources/loginData.xml")
    public void isLetterReceivedTest(ArrayList<LoginObject> obj) throws InterruptedException {
        LoginObject user1 = obj.get(0);
        LoginObject user2 = obj.get(1);

        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login(user1.username, user1.password);
        homePage.sendLetter(user2.email, topic, body);
        loginPage = homePage.logout();

        loginPage.login(user2.username, user2.password);
        Assert.assertTrue(homePage.isLetterReceived(user1.email, topic, body), "Letter doesn't exist in inbox backet");
        loginPage = homePage.logout();
    }

    @Test(dataProvider = "loginData", dataProviderClass=XmlLoginHelper.class)
    @XmlFileParameters(path = "src/main/resources/loginData.xml")
    public void isLetterSentTest(ArrayList<LoginObject> obj) {
        LoginObject user1 = obj.get(0);
        LoginObject user2 = obj.get(1);

        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login(user1.username, user1.password);
        homePage.sendLetter(user2.email, topic, body);
        Assert.assertTrue(homePage.isLetterSent(user2.email, topic, body), "Letter doesn't exist in sent backet");
        loginPage = homePage.logout();
    }

    @Test(dataProvider = "loginData", dataProviderClass=XmlLoginHelper.class)
    @XmlFileParameters(path = "src/main/resources/loginData.xml")
    public void isLetterRemovedTest(ArrayList<LoginObject> obj) throws InterruptedException {
        LoginObject user1 = obj.get(0);
        LoginObject user2 = obj.get(1);

        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = loginPage.login(user1.username, user1.password);
        homePage.sendLetter(user2.email, topic, body);
        homePage.removeSentLetter(user2.email, topic, body);

        Assert.assertTrue(homePage.isLetterInTrash(user1.email, topic, body), "Letter doesn't exist in trash backet" );
        loginPage = homePage.logout();
    }
}
