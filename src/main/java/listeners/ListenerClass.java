package listeners;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IlyaMakarov on 1/27/2017.
 */
public class ListenerClass extends TestListenerAdapter {

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] captureScreenshot(WebDriver d) {
        return ((TakesScreenshot) d).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment
    public String runConfigurations(WebDriver d) {
        StringBuilder result = new StringBuilder();
        Capabilities cap = ((RemoteWebDriver) d).getCapabilities();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        result.append("Date:" + dateFormat.format(date) + "\n");
        result.append("Browser:" + cap.getBrowserName() + "\n");
        result.append("Browser version:" + cap.getVersion() + "\n");
        result.append("Platform:" + cap.getPlatform() + "\n");
        return result.toString();
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Object webDriverAttribute = tr.getTestContext().getAttribute("WebDriver");
        if (webDriverAttribute instanceof WebDriver) {
            captureScreenshot((WebDriver) webDriverAttribute);
            runConfigurations((WebDriver) webDriverAttribute);
        }
    }
}