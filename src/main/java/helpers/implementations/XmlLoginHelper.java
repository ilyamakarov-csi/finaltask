package helpers.implementations;

import helpers.annotations.XmlFileParameters;
import objects.LoginObject;
import org.testng.annotations.DataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by IlyaMakarov on 1/20/2017.
 */
public class XmlLoginHelper {
    private final static String USERNAME_XML = "username";
    private final static String PASSWORD_XML = "password";
    private final static String USER_ID_XML = "userid";
    private final static String USER_XML = "user";
    private final static String EMAIL_XML = "email";

    private static Object[][] loadDataFromXmlFile(final String path, final String encoding) throws ParserConfigurationException, IOException, SAXException {
        File file = new File(path);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        NodeList nList = document.getElementsByTagName(USER_XML);
        ArrayList<LoginObject> loginData = new ArrayList<LoginObject>();

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                LoginObject credentials = new LoginObject();

                credentials.userID = Integer.parseInt(eElement.getElementsByTagName(USER_ID_XML).item(0).getTextContent());
                credentials.username = eElement.getElementsByTagName(USERNAME_XML).item(0).getTextContent();
                credentials.password = eElement.getElementsByTagName(PASSWORD_XML).item(0).getTextContent();
                credentials.email = eElement.getElementsByTagName(EMAIL_XML).item(0).getTextContent();

                loginData.add(credentials);
            }
        }

        return new Object[][] { new Object[] { loginData } } ;
    }

    @DataProvider(name = "loginData")
    public static Object[][] getDataFromXml(final Method testMethod) throws ParserConfigurationException, IOException, SAXException {
        XmlFileParameters parameters = testMethod.getAnnotation(XmlFileParameters.class);

        if (parameters != null) {
            String path = parameters.path();
            String encoding = parameters.encoding();
            return loadDataFromXmlFile(path, encoding);
        } else {
            throw new RuntimeException("Couldn't find the annotation");
        }
    }
}
