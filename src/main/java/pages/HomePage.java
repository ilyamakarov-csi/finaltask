package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by IlyaMakarov on 1/18/2017.
 */
public class HomePage {
    private final static String URL = "https://mail.google.com/mail/u/0/#inbox";
    private By inboxBacket = By.xpath(".//a[contains(@href, 'https://mail.google.com/mail/u/0/#inbox')]");
    private By sentBacket = By.xpath(".//a[contains(@href, 'https://mail.google.com/mail/u/0/#sent')]");
    private By trashBacket = By.xpath(".//a[contains(@href, 'https://mail.google.com/mail/u/0/#trash')]");

    private By signOutWindow = By.xpath(".//a[contains(@href, 'https://accounts.google.com/SignOutOptions')]");
    private By signOutLink = By.xpath(".//a[contains(@href, 'https://accounts.google.com/Logout')]");

    private By createLetterButton = By.xpath(".//div[@class='T-I J-J5-Ji T-I-KE L3']");
    private By addressLetter = By.cssSelector("form[enctype='multipart/form-data'] textarea:nth-of-type(1)");
    private By topicLetter = By.cssSelector("form[enctype='multipart/form-data'] input[name='subjectbox']");
    private By bodyLetter = By.cssSelector("table div.Am.Al.editable.LW-avf");
    private By sendEmailButton = By.xpath(".//table//div[contains(@class, 'T-I J-J5-Ji aoO T-I-atl L3')]");

    private By removeLetterButton = By.cssSelector("div.D:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3)");
    private By moreButton = By.xpath(".//span[@class='ait']/ancestor::span");

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        driver.get(URL);
    }

    public boolean isInboxBacketDisplayed() {
        return driver.findElement(inboxBacket).isDisplayed();
    }

    public LoginPage logout() {
        driver.findElement(signOutWindow).click();
        driver.findElement(signOutLink).click();

        WebDriverWait wait = new WebDriverWait(driver, 15);
        try {
            if(wait.until(ExpectedConditions.alertIsPresent()) != null) {
                Alert alert = driver.switchTo().alert();
                alert.accept();
            }
        }
        catch (TimeoutException e) {

        }

        return new LoginPage(driver);
    }

    public void sendLetter(String whom, String topic, String body) {
        driver.findElement(createLetterButton).click();
        driver.findElement(addressLetter).sendKeys(whom);
        driver.findElement(topicLetter).sendKeys(topic);
        driver.findElement(bodyLetter).sendKeys(body);
        driver.findElement(sendEmailButton).click();
    }

    public void removeSentLetter(String whom, String topic, String body) {//, Date date) {
        driver.findElement(sentBacket).click();

        processRemovingLetter(whom, topic, body);//, date);
    }


    public boolean isLetterSent(String whom, String topic, String body) {
        driver.findElement(sentBacket).click();

        return isLetterExist(whom, topic, body);
    }

    public boolean isLetterReceived(String whom, String topic, String body) {//, Date date) {
        driver.findElement(inboxBacket).click();

        return isLetterExist(whom, topic, body);
    }

    public boolean isLetterInTrash(String from, String topic, String body) {//, Date date) {
        WebElement buttonMore = driver.findElement(moreButton);//.click();
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", buttonMore);

        driver.findElement(trashBacket).click();

        return isLetterExist(from, topic, body);
    }

    private By getEmailAddress(String whom) {
        return By.xpath(".//div[@role = 'main']//table//span[@email = '" + whom + "' and not(ancestor::div[@class='afn'])]");
    }

    private By getTopic(String topic) {
        return By.xpath(".//div[@role = 'main']//*[self::span|self::b][contains(text(),'" + topic +"')]");
    }

    private By getBody(String body) {
        return By.xpath(".//div[@role = 'main']//span[contains(text(),'" + body + "')]");
    }

    private By getLetterDate(Date date) {
        return By.cssSelector(".//div[@role = 'main']//*[contains(text(),'" + date + "')]");
    }

    private void processRemovingLetter(String whom, String topic, String body) {//, Date date) {
        getRemoveLetterButton(whom, topic, body).click();//, date).click();

        driver.findElement(removeLetterButton).click();
        driver.findElement(By.cssSelector("button[name='ok']")).click();
    }

    private WebElement getRemoveLetterButton(String whom, String topic, String body) {//, Date date) {
        return driver.findElement(By.xpath("//*[self::span|self::b][@email='" + whom +"']//ancestor::tr//*[self::span|self::b][text()='" + topic +
                "']//ancestor::tr//div[@role='checkbox']"));
    }

    private boolean isLetterExist(final String whom, final String topic, final String body) {//, Date date) {
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);

        fluentWait.until(ExpectedConditions.presenceOfElementLocated(getTopic(topic)));

        WebElement topicElement = driver.findElement(getTopic(topic));
        WebElement bodyElement = driver.findElement(getBody(body));
        WebElement emailAddressElement = driver.findElement(getEmailAddress(whom));

        return emailAddressElement.isDisplayed() && topicElement.isDisplayed() &&
                bodyElement.isDisplayed(); //&& dateElement.isDisplayed();
    }

}
