package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by IlyaMakarov on 1/18/2017.
 */
public class LoginPage {
    private static final String URL = "https://accounts.google.com/AccountChooser";

    private By LINK_ADD_ACCOUNT = By.id("account-chooser-add-account");
    private By LINK_CHANGE_ACCOUNT = By.id("account-chooser-link");
    private By USERNAME_INPUT = By.id("Email");
    private By PASSWORD_INPUT = By.id("Passwd");
    private By SUBMIT_USERNAME_BUTTON = By.id("next");
    private By SUBMIT_PASSWORT_BUTTON = By.id("signIn");

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        driver.get(URL);
    }

    public boolean isLinkAddAccountDisplayed() {
        return driver.findElement(LINK_ADD_ACCOUNT).isDisplayed();
    }

    public boolean isLinkChangeAccountDisplayed() {
        return driver.findElement(LINK_CHANGE_ACCOUNT).isDisplayed();
    }

    public boolean isUserNameInputDisplayed() {
        return driver.findElement(USERNAME_INPUT).isDisplayed();
    }

    public boolean isPasswordInputDisplayed() {
        return driver.findElement(PASSWORD_INPUT).isDisplayed();
    }

    public HomePage login(String username, String password) {
        if(driver.findElements(LINK_ADD_ACCOUNT).size() > 0)
            driver.findElement(LINK_ADD_ACCOUNT).click();
        else if(driver.findElements(LINK_CHANGE_ACCOUNT).size() > 0)
            driver.findElement(LINK_CHANGE_ACCOUNT).click();

        driver.findElement(USERNAME_INPUT).sendKeys(username);
        driver.findElement(SUBMIT_USERNAME_BUTTON).click();

        driver.findElement(PASSWORD_INPUT).sendKeys(password);
        driver.findElement(SUBMIT_PASSWORT_BUTTON).click();

        return new HomePage(driver);
    }

}
